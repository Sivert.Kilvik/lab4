package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        if (rows <= 0) {
            throw new IllegalArgumentException("the grid can't have less than 1 row, you tried to make one with " + rows + " rows.");}
        this.rows = rows;

        if (columns <= 0) {
            throw new IllegalArgumentException("the grid can't have less than 1 column, you tried to make one with " + columns + " columns.");}
        this.cols = columns;
        this.grid = new CellState[rows][cols];
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                grid[r][c] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || column < 0 || row >= rows || column >= cols) {
            throw new IndexOutOfBoundsException("Illegal grid index.");
        }
        grid[row][column] = element;
    }


    @Override
    public CellState get(int row, int column) {
        if (row < 0 || column < 0 || row >= rows || column >= cols) {
            throw new IndexOutOfBoundsException("Illegal grid index.");
        }
        return grid[row][column];

    }

    @Override
    public IGrid copy() {
        IGrid clone = new CellGrid(rows, cols, CellState.DEAD);
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                clone.set(r, c, get(r, c));
            }
        }
        return clone;
    }
}