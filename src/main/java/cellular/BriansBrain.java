package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid brian;

    public BriansBrain(int rows, int columns) {
        brian = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return brian.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random rand = new Random();
		for (int row = 0; row < brian.numRows(); row++) {
			for (int col = 0; col < brian.numColumns(); col++) {
				if (rand.nextInt(3) == 0) {
					brian.set(row, col, CellState.ALIVE);
				} else if(rand.nextBoolean()){
					brian.set(row, col, CellState.DYING);
				} else {
					brian.set(row, col, CellState.DEAD);
				}
			}
		}

    }

    @Override
    public void step() {
        IGrid nextBrian = brian.copy();
		for (int r = 0; r < brian.numRows(); r++) {
			for (int c = 0; c < brian.numColumns(); c++) {
				nextBrian.set(r, c, getNextCell(r, c));
			}
		}
		brian = nextBrian;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        if(getCellState(row, col) == CellState.ALIVE)
			return CellState.DYING;
		else if (getCellState(row, col) == CellState.DYING)
			return CellState.DEAD;
		if(countNeighbors(row, col) == 2)
			return CellState.ALIVE;
		else
			return CellState.DEAD;

    }

    @Override
    public int numberOfRows() {
		return brian.numRows();
    }

    @Override
    public int numberOfColumns() {
        return brian.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return brian;
    }

    private int countNeighbors(int row, int col) {
		int neighborsAlive = 0;
		for (int r = (row - 1); r < (row + 2); r++) {
			for (int c = (col - 1); c < (col + 2); c++) {
				if (c < 0 || r < 0 || r >= numberOfRows() || c >= numberOfColumns())
					continue;
				if ((r != row || c != col) && getCellState(r, c) == CellState.ALIVE)
                    neighborsAlive++;
			}
		}
		return neighborsAlive;
	}

}
